import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Column(
        children: [
          Container(
            height: 200,
            width: 200,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(100.0),
              border: Border.all(
                width: 1.0,
                style: BorderStyle.solid,
              ),
              image: DecorationImage(
                fit: BoxFit.cover,
                image: AssetImage('images/Chompoo.JPG'),
              ),
            ),
          ),
          Container(
            child: Column(
              children: [
                Text(
                  'Tidarat Pawapattarakul (Chompoo)',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  'Female 22 Y',
                  style: TextStyle(
                    fontWeight: FontWeight.w100,
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );

    Widget skillsSection = Container(
      child: Row(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Row(children: [
              Text('SKILLS',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            ]),
          ),
          Container(
            padding: EdgeInsets.all(30),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset('images/html.png',
                    width: 50, height: 50, fit: BoxFit.cover),
                Image.asset('images/css.png',
                    width: 35, height: 50, fit: BoxFit.fill),
                Image.asset('images/Javascript.png',
                    width: 45, height: 50, fit: BoxFit.fill),
                Image.asset('images/Vue.png',
                    width: 50, height: 40, fit: BoxFit.cover),
                Image.asset('images/Nodejs.png',
                    width: 60, height: 50, fit: BoxFit.fill),
                Image.asset('images/Java.png',
                    width: 70, height: 50, fit: BoxFit.fill),
              ],
            ),
          )
        ],
      ),
    );

    Widget contactSection = Container(
      child: Column(
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Row(children: [
              Text('CONTACT',
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
            ]),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Icon(
                      Icons.place,
                      color: Colors.red[900],
                      size: 30.0,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      ' ADDREASS',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(' Chonburi , Thailand',
                        style: TextStyle(color: Colors.grey[700])),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Icon(
                      Icons.cake,
                      color: Colors.pinkAccent[400],
                      size: 30.0,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      ' DATE OF BIRTH',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(' 09 JUN 1999',
                        style: TextStyle(color: Colors.grey[700])),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Icon(
                      Icons.call,
                      color: Colors.blue,
                      size: 30.0,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      ' PHONE',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text(' 099-XXXXXXX ',
                        style: TextStyle(color: Colors.grey[700])),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Icon(
                      Icons.email,
                      color: Colors.blue[900],
                      size: 30.0,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      ' E-MAIL',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    Text(' 61160144@go.buu.ac.th',
                        style: TextStyle(color: Colors.grey[700])),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );

    Widget profileSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text('PROFILE',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          ),
          Container(
            padding: EdgeInsets.fromLTRB(32, 0, 20, 0),
            child: Text(
              '   My name is Tidarat pawapattarakul. I’m from Thailand. '
              ' I live in Chonburi. '
              ' Chonburi is in the east of Thailad. '
              ' I’m twenty-two years old. '
              ' I come from a relatively small family.'
              'There are three people in my family.'
              ' My mother name is Malee.'
              ' Father name is Phichet'
              ' i have one brother His'
              ' name is Sirichai and he is twenty-five. '
              ' we have one dog',
              softWrap: true,
            ),
          )
        ],
      ),
    );

    Widget educatioSection = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.all(20),
            child: Text('Education',
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 20)),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset(
                      'images/Buu-logo11.png',
                      width: 80,
                      height: 80,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      '  2018 - Now',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('  Bachelor of Science (Computer Science)',
                        style: TextStyle(color: Colors.grey[700])),
                  ],
                )
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(20),
            child: Row(
              children: [
                Column(
                  children: [
                    Image.asset(
                      'images/janLogo.png',
                      width: 80,
                      height: 90,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  textBaseline: TextBaseline.alphabetic,
                  children: [
                    Text(
                      '  2015 - 1017',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Text('  High school-science-Math',
                        style: TextStyle(color: Colors.grey[700])),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );

    return MaterialApp(
      title: 'Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Resume'),
        ),
        body: ListView(children: [
          titleSection,
          skillsSection,
          contactSection,
          profileSection,
          educatioSection
        ]),
        backgroundColor: Colors.amberAccent[100],
      ),
    );
  }
}
